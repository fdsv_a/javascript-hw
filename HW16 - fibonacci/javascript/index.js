"use strict";

const validNumber = (question) => {
  let num;
  do {
    num = prompt(question);
  } while (!num || isNaN(num));
  return num;
};
const F0 = +validNumber("Введите первое число последовательности Фибоначчи");
const F1 = +validNumber("Введите второе число последовательности Фибоначчи");
const n = +validNumber("Введите порядковый номер числа Фибоначчи");

const getFibonacci = (F0, F1, n) => {
  if (n === 1) {
    return F0;
  }
  if (n === 2) {
    return F1;
  }
  if (n > 2) {
    return getFibonacci(F0, F1, n - 1) + getFibonacci(F0, F1, n - 2);
  }
};
console.log(getFibonacci(F0, F1, n));
