const url = "https://ajax.test-danit.com/api/swapi/films";
let body = document.querySelector("body");
const preloader = document.querySelector(".preloader");

const sendRequest = (method, someUrl) => {
  const xhr = new XMLHttpRequest();

  return new Promise((resolve, reject) => {
    xhr.open(method, someUrl);
    xhr.responseType = "json";
    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject(xhr.response);
      } else {
        resolve(xhr.response);
      }
    };
    xhr.onerror = () => {
      console.log("ERROR");
    };
    xhr.send();
  });
};

sendRequest("GET", url)
  .then((data) => {
    data.forEach((elem) => {
      let {
        episodeId: episodeVal,
        name: nameVal,
        openingCrawl: crawlVal,
        characters: charactersVal,
      } = elem;
      // console.log(charactersVal);
      let span = document.createElement("span");
      body.insertAdjacentElement("beforeend", span);
      let episode = document.createElement("p");
      let name = document.createElement("p");
      let crawl = document.createElement("p");
      span.insertAdjacentElement("beforeend", episode);
      span.insertAdjacentElement("beforeend", name);
      span.insertAdjacentElement("beforeend", crawl);
      episode.insertAdjacentText("beforeend", episodeVal);
      name.insertAdjacentText("beforeend", nameVal);
      crawl.insertAdjacentText("beforeend", crawlVal);
      let actor = document.createElement("p");
      name.insertAdjacentElement("afterend", actor);

      charactersVal.forEach((elem) => {
        sendRequest("GET", elem).then((actorInfo) => {
          let { name: nameVal } = actorInfo;
          actor.insertAdjacentText("beforeend", `${nameVal}, `);
        });
      });
    });
    console.log(data);
  })
  .catch((err) => console.log(err));
