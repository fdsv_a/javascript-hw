class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  getName() {
    console.log(this._name);
  }
  setName(name) {
    this.name = name;
  }
  getAge() {
    console.log(this.age);
  }
  setAge(age) {
    this.age = age;
  }
  getSalary() {
    console.log(this._salary);
  }
  setSalary(salary) {
    this.salary = salary;
  }
}

class Programmer extends Employee {
  constructor(lang, name, age, salary) {
    super(name, age, salary);
    this.lang = lang;
  }
  setName(name) {
    super.setName(name);
  }
  getName() {
    super.getName();
  }
  setAge(age) {
    super.setAge(age);
  }
  getAge() {
    super.getAge();
  }
  setSalary(salary) {
    super.setSalary(salary);
  }
  getSalary() {
    console.log(this.salary * 3);
  }
}

const julia = new Programmer(["eng", "rus", "ukr"], "Julia", 25, 2000);
console.log(julia);
const bob = new Programmer(["deu", "rus", "ukr"], "Bob", 27, 3000);
console.log(bob);
const max = new Programmer(["eng", "deu", "ukr"], "Max", 20, 4000);
console.log(max);
