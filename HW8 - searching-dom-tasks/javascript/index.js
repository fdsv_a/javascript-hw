// // Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const p = document.querySelectorAll("p");
for (let item of p) {
  item.style.backgroundColor = "#ff0000";
}

// // Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
childNodes = optionsList.childNodes;
for (let item of childNodes) {
  console.log(`${item.nodeName} : ${item.nodeType}`);
}

// // Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

document.getElementById("testParagraph").innerText = "This is a paragraph";

// // Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const mainHeaderChildren = document.querySelector(".main-header").children;
for (let item of mainHeaderChildren) {
  console.log(item);
  item.classList.add("nav-item");
}

// // Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitle = document.querySelectorAll(".section-title");
for (let item of sectionTitle) {
  item.classList.remove("section-title");
}
