const urlUsers = "https://ajax.test-danit.com/api/json/users";
const urlPosts = "https://ajax.test-danit.com/api/json/posts";
const urlDelete = "https://ajax.test-danit.com/api/json/posts/";
const list = document.querySelector(".list");

class Card {
  constructor() {
    this.name = document.createElement("p");
    this.email = document.createElement("p");
    this.title = document.createElement("p");
    this.text = document.createElement("p");
    this.item = document.createElement("li");
    this.btnDelete = document.createElement("button");
  }

  createElement(name, email, title, text, postId) {
    this.item.className = "item";
    this.btnDelete.className = "delete";
    this.item.id = postId;
    this.btnDelete.id = postId;
    this.name.className = "name";
    this.email.className = "email";
    this.title.className = "title";
    this.text.className = "text";
    list.append(this.item);

    this.item.insertAdjacentElement("afterbegin", this.btnDelete);
    this.item.insertAdjacentElement("afterbegin", this.text);
    this.item.insertAdjacentElement("afterbegin", this.title);
    this.item.insertAdjacentElement("afterbegin", this.email);
    this.item.insertAdjacentElement("afterbegin", this.name);

    this.btnDelete.innerText = "Delete";
    this.name.innerText = name;
    this.email.innerText = email;
    this.title.innerText = title;
    this.text.innerText = text;
  }
}

const sendRequest = (method, someUrl) => {
  const xhr = new XMLHttpRequest();

  return new Promise((resolve, reject) => {
    xhr.open(method, someUrl);
    xhr.responseType = "json";
    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject(xhr.response);
      } else {
        resolve(xhr.response);
      }
    };
    xhr.onerror = () => {
      console.log("ERROR");
    };
    xhr.send();
  });
};

sendRequest("GET", urlUsers).then((users) => {
  sendRequest("GET", urlPosts)
    .then((posts) => {
      document.querySelector(".lds-default").classList.add("hide");
      posts.forEach((elem) => {
        let {
          userId: userIdVal,
          title: titleVal,
          body: bodyVal,
          id: postIdVal,
        } = elem;

        users.forEach((elem) => {
          let { id: idVal, name: nameVal, email: emailVal } = elem;
          if (userIdVal === idVal) {
            let card = new Card();
            card.createElement(nameVal, emailVal, titleVal, bodyVal, postIdVal);
          }
        });
      });
    })
    .then(() => {
      const btnDelete = document.querySelectorAll(".delete");
      btnDelete.forEach((elem) => {
        elem.addEventListener("click", () => {
          sendRequest("DELETE", `${urlDelete}${elem.id}`).then(() => {
            document.querySelectorAll(`.item`).forEach((e) => {
              if (e.id === elem.id) {
                e.remove();
              }
            });
          });
        });
      });
    });
});
