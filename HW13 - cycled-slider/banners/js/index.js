"use strict";

const imgCollection = Array.from(document.querySelectorAll(".image-to-show"));

const btnContainer = document.createElement("div");
btnContainer.classList.add(".btn-container");
const btnStop = document.createElement("button");
const btnContinue = document.createElement("button");
btnStop.innerText = "Припинити";
btnContinue.innerText = "Відновити показ";
document.querySelector(".images-wrapper").append(btnContainer);
btnContainer.append(btnStop);
btnContainer.append(btnContinue);
btnContainer.classList.add("btn-container");
btnStop.classList.add("btn-stop");
btnContinue.classList.add("btn-continue");
let timerID;
let num;
let globalDelay;
const showImg = (i, delay) => {
  imgCollection.forEach((e) => {
    e.classList.remove("active");
  });
  imgCollection[i % imgCollection.length].classList.add("active");
  num = i;
  globalDelay = delay;

  timerID = setTimeout(() => {
    showImg(i + 1, delay);
  }, delay);
};

showImg(0, 3000);

btnStop.addEventListener("click", () => {
  clearTimeout(timerID);
});
btnContinue.addEventListener("click", () => {
  showImg(num, globalDelay);
});
