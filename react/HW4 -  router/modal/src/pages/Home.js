import React from "react";
import ListGoods from "../components/ListGoods/ListGoods";
import Modal from "../components/Modal/Modal";
import PropTypes from "prop-types";
import { setModalApp } from "../store/appReducer/actionCreators";
import { useSelector, useDispatch } from "react-redux";

const Home = (props) => {
  const {
    goods,
    addToCart,
    addToFavorite,
    deleteFavorite,
  } = props;
  const modal = useSelector((store) => store.appReducer.modal);
  const dispatch = useDispatch();

  return (
    <>
      <ListGoods
        goods={goods}
        addToFavorite={addToFavorite}
        addToCart={addToCart}
        deleteFavorite={deleteFavorite}
        deleteCart={false}
        addCart={true}
      />
      {modal && (
        <Modal
          header="Do you want to add this item to your shopping cart?"
          closeButton={true}
          text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, animi nisi dolorum illum culpa quae placeat rem numquam a consequatur reprehenderit facere voluptas nostrum corporis aut eaque dolor sunt aperiam?"
          actions={
            <>
              <button
                onClick={() => {
                  addToCart(modal);
                  dispatch(setModalApp(""));
                }}
              >
                Ok
              </button>
              <button onClick={()=>(dispatch(setModalApp("")))}>Cancel</button>
            </>
          }
          closeModal={()=>(dispatch(setModalApp("")))}
        />
      )}
    </>
  );
};

Home.propTypes = {
  goods: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      image: PropTypes.string,
      isFavourite: PropTypes.bool,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    })
  ),
  addToCart: PropTypes.func.isRequired,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
};
Home.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};

export default Home;
