import React from "react";
import ListGoods from "../components/ListGoods/ListGoods";
import Modal from "../components/Modal/Modal";
import PropTypes from "prop-types";
import { setModalApp } from "../store/appReducer/actionCreators";
import { useSelector, useDispatch } from "react-redux";


const Favorites = (props) => {
  const {
    goods,
    addToFavorite,
    deleteFavorite,
    addToCart,
  } = props;
  const modal = useSelector((store) => store.appReducer.modal);
  const dispatch = useDispatch();

  return localStorage.getItem('favoriteCounter') ? (
    <>
      <ListGoods
        goods={goods.filter(
          (e) =>
            localStorage.getItem(e.id) &&
            localStorage.getItem(e.id).includes("favorite")
        )}
        addToFavorite={addToFavorite}
        deleteFavorite={deleteFavorite}
        addToCart={addToCart}
        deleteCart={false}
        addCart={true}
      />
      {modal && (
        <Modal
          header="Do you want to add this item to your shopping cart?"
          closeButton={true}
          text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, animi nisi dolorum illum culpa quae placeat rem numquam a consequatur reprehenderit facere voluptas nostrum corporis aut eaque dolor sunt aperiam?"
          actions={
            <>
              <button
                onClick={() => {
                  addToCart(modal);
                  dispatch(setModalApp(''))
                }}
              >
                Ok
              </button>
              <button onClick={()=>(dispatch(setModalApp("")))}>Cancel</button>
            </>
          }
          closeModal={()=>(dispatch(setModalApp("")))}
        />
      )}
    </>
  ) : (
    <h1>Sorry, your favorites list is empty</h1>
  );
};

Favorites.propTypes = {
  goods: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      image: PropTypes.string,
      isFavourite: PropTypes.bool,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    })
  ),
  addToCart: PropTypes.func.isRequired,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
};
Favorites.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};

export default Favorites;
