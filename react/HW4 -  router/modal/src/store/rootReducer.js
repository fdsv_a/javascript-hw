import {combineReducers} from 'redux';
import appReducer from './appReducer/appReducer';

const rootReducer = combineReducers({
appReducer,
})
export default rootReducer;