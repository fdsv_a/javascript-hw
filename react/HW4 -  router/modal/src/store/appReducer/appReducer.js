import { GET_APP, SET_IS_LOADING_APP, SET_MODAL_APP } from "./action";
import produce from "immer";

const initialState = {
  data: [],
  isLoading: false,
  modal: "",
};
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_APP: {
      return produce(state, (draftState) => {
        draftState.data = action.payload;
      });

    }
    case SET_IS_LOADING_APP: {
      return produce(state, (draftState) => {
        draftState.isLoading = action.payload;
      });
    }
    case SET_MODAL_APP: {
      return produce(state, (draftState) => {
        draftState.modal = action.payload;
      });
    }
    default: {
      return state;
    }
  }
};

export default appReducer;
