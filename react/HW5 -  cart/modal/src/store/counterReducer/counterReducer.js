import { CHANGE_CART_COUNTER, CHANGE_FAVORITE_COUNTER } from "./action";
import produce from "immer";

const initialState = {
  cartCounter: localStorage.getItem("cartCounter"),
  favoriteCounter: localStorage.getItem("favoriteCounter"),
};
const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_CART_COUNTER: {
        return produce(state, (draftState) => {
          draftState.cartCounter = action.payload;
        });
      }
      case CHANGE_FAVORITE_COUNTER: {
        return produce(state, (draftState) => {
          draftState.favoriteCounter = action.payload;
        });
      }
    default: {
      return state;
    }
  }
};

export default counterReducer;
