import {
    CHANGE_CART_COUNTER,CHANGE_FAVORITE_COUNTER,
  } from "./action";
  
  
  export const changeCartCounter = (payload) => (dispatch) => {
    dispatch({ type: CHANGE_CART_COUNTER, payload: payload });
  };
  export const changeFavoriteCounter = (payload) => (dispatch) => {
    dispatch({ type: CHANGE_FAVORITE_COUNTER
        , payload: payload });
  };
  