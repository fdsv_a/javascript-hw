import { combineReducers } from "redux";
import appReducer from "./appReducer/appReducer";
import counterReducer from "./counterReducer/counterReducer";

const rootReducer = combineReducers({
  appReducer,
  counterReducer,
});
export default rootReducer;
