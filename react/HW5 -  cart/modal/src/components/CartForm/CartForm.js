import React from "react";
import { Form, Formik } from "formik";
import * as yup from "yup";
import CustomInput from "../CustomInput";
import styles from "./CartForm.module.scss";
import { useDispatch } from "react-redux";
import { changeCartCounter } from "../../store/counterReducer/actionCreators";
import { setCartForm } from "../../store/appReducer/actionCreators";

const CartForm = (props) => {
  const dispatch = useDispatch();
  const { goods } = props;
  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    address: "",
    phoneNumber: "",
  };
  const handleSubmit = (values, { resetForm }) => {
    localStorage.removeItem("cartCounter");
    dispatch(changeCartCounter(0));
    dispatch(setCartForm(false));
    goods.forEach((e) => {
      localStorage.getItem(e.id).includes("cart") &&
      localStorage.getItem(e.id).includes("favorite")
        ? (localStorage[e.id] = "favorite")
        : localStorage.removeItem(e.id);
    });
    console.log(goods);
    alert("Thanks for your order");
    resetForm();
  };

  const schema = yup.object().shape({
    firstName: yup
      .string()
      .required("It is a required field")
      .min(2, "Min 2 characters")
      .max(24, "Max 24 characters")
      .matches(/[A-Za-z]/g, "Should contain only characters"),
    lastName: yup
      .string()
      .required("It is a required field")
      .min(2, "Min 2 characters")
      .max(24, "Max 24 characters")
      .matches(/[A-Za-z]/g, "Should contain only characters"),
    age: yup
      .number()
      .required("It is a required field")
      .min(8, "Age should be greater then 8")
      .max(110, "Age should be lesser then 110"),
    address: yup.string().required("It is a required field"),

    phoneNumber: yup
      .string()
      .required("It is a required field")

      .min(13, "That doesn't look like a phone number")
      .max(13, "That doesn't look like a phone number")
      .matches(/(\+\d)\w+/g, "That doesn't look like a phone number"),
  });

  return (
    <>
      <h1 className="cartFormTitle">Go to the ordering</h1>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={schema}
      >
        {({ isValid }) => (
          <Form className={styles.form}>
            <CustomInput
              name="firstName"
              type="text"
              placeholder="First name"
            />
            <CustomInput name="lastName" type="text" placeholder="Last name" />
            <CustomInput name="age" type="text" placeholder="Age" />
            <CustomInput name="address" type="text" placeholder="Address" />
            <CustomInput
              name="phoneNumber"
              type="text"
              placeholder="+380000000000"
            />
            <button type="submit" disabled={!isValid}>
              Checkout
            </button>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default CartForm;
