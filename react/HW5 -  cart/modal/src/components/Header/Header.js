import React from "react";
import PropTypes from "prop-types";
import Navigation from "../Navigation/Navigation";

const Header = () => {
  return <Navigation />;
};
Header.propTypes = {
  cartCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  favoriteCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
Header.defaultProps = {
  cartCounter: localStorage.getItem("cartCounter"),

  favoriteCounter: localStorage.getItem("favoriteCounter"),
};

export default Header;
