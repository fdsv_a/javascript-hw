import React from "react";
import styles from './Button.module.scss';

class Button extends React.PureComponent {
  render() {
    console.log(this);
    const {text, onClick, backColor } = this.props;
    return <button style={{ backgroundColor: backColor}} onClick={onClick} type='button'>{text}</button>;
  }
}

export default Button;
