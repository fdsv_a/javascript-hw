import React from "react";
import styles from "./Modal.module.scss";

class Modal extends React.PureComponent {
  render() {
    const { header, closeButton, text, actions, closeModal } = this.props;
    return (
      <div
        className={styles.container}
        onClick={(e) => {
          e.target === e.currentTarget && closeModal();
        }}
      >
        <div className={styles.modal}>
          <div className={styles.header}>
            <div>{header}</div>
            {closeButton && <div onClick={closeModal}><span>&#10006;</span></div>}
          </div>
          <div className={styles.text}>{text}</div>
          <div className={styles.button}>{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
