import React from "react";
import styles from "./App.module.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.PureComponent {
  state = {
    showFirstModal: false,
    showSecondModal: false,
  };
  render() {
    const {
      closeModal = () => {
        this.setState({
          showFirstModal: false,
          showSecondModal: false,
        });
      },
    } = this.props;

    return (
      <div className={styles.App}>
        <div className={styles.container}>
          <Button
            onClick={() => {
              this.setState({
                showFirstModal: true,
                showSecondModal: false,
              });
            }}
            text="Open first modal"
            backColor="#a8c4f8"
          ></Button>
          <Button
            onClick={() => {
              this.setState({
                showFirstModal: false,
                showSecondModal: true,
              });
            }}
            text="Open second modal"
            backColor="#a8f8c0"
          ></Button>
        </div>
        {this.state.showFirstModal && (
          <Modal
            header="Вы хотите удалить этот файл?"
            closeButton={true}
            text="После того, как вы удалите этот файл, отменить это действие будет невозможно. Вы уверены, что хотите удалить его?"
            actions={
              <>
                <button onClick={closeModal}>Oк</button>
                <button onClick={closeModal}>Отмена</button>
              </>
            }
            closeModal={closeModal}
          />
        )}
        {this.state.showSecondModal && (
          <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            text="Once you delete this file, it won`t be possible to undo this action. Are you sure eou want to delete it?"
            actions={
              <>
                <button onClick={closeModal}>Ok</button>
                <button onClick={closeModal}>Cancel</button>
              </>
            }
            closeModal={closeModal}
          />
        )}
      </div>
    );
  }
}

export default App;
