import React from "react";
import ListGoods from "../components/ListGoods/ListGoods";
import Modal from "../components/Modal/Modal";
import PropTypes from "prop-types";

const Cart = (props) => {
  const {
    goods,
    addToFavorite,
    deleteFavorite,
    addToCart,
    deleteFromCart,
    showModal,
    closeModal,
    modal,
  } = props;
  return localStorage.getItem('cartCounter') ? (
    <>
      <ListGoods
        goods={goods.filter(
          (e) =>
            localStorage.getItem(e.id) &&
            localStorage.getItem(e.id).includes("cart")
        )}
        addToFavorite={addToFavorite}
        deleteFavorite={deleteFavorite}
        addToCart={addToCart}
        deleteFromCart={deleteFromCart}
        showModal={showModal}
        closeModal={closeModal}
        deleteCart={true}
        addCart={false}
      />
      {modal && (
        <Modal
          header="Do you want to delete this item from your shopping cart?"
          closeButton={true}
          text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, animi nisi dolorum illum culpa quae placeat rem numquam a consequatur reprehenderit facere voluptas nostrum corporis aut eaque dolor sunt aperiam?"
          actions={
            <>
              <button
                onClick={() => {
                  deleteFromCart(modal);
                  closeModal();
                }}
              >
                Ok
              </button>
              <button onClick={closeModal}>Cancel</button>
            </>
          }
          closeModal={closeModal}
        />
      )}
    </>
  ) : (
    <h1>Sorry, your cart is empty</h1>
  );
};

Cart.propTypes = {
  goods: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      image: PropTypes.string,
      isFavourite: PropTypes.bool,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    })
  ),
  addToCart: PropTypes.func.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,
  modal: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
Cart.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};

export default Cart;
