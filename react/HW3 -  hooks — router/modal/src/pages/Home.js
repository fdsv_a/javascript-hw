import React from "react";
import ListGoods from "../components/ListGoods/ListGoods";
import Modal from "../components/Modal/Modal";
import PropTypes from "prop-types";


const Home = (props) => {
  const {
    goods,
    addToCart,
    addToFavorite,
    deleteFavorite,
    showModal,
    closeModal,
    modal,
  } = props;

  return (
    <>
      <ListGoods
        goods={goods}
        addToFavorite={addToFavorite}
        addToCart={addToCart}
        deleteFavorite={deleteFavorite}
        showModal={showModal}
        closeModal={closeModal}
        modal={modal}
        deleteCart={false}
        addCart={true}
      />
      {modal && (
        <Modal
          header="Do you want to add this item to your shopping cart?"
          closeButton={true}
          text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, animi nisi dolorum illum culpa quae placeat rem numquam a consequatur reprehenderit facere voluptas nostrum corporis aut eaque dolor sunt aperiam?"
          actions={
            <>
              <button
                onClick={() => {
                  addToCart(modal);
                  closeModal();
                }}
              >
                Ok
              </button>
              <button onClick={closeModal}>Cancel</button>
            </>
          }
          closeModal={closeModal}
        />
      )}
    </>
  );
};

Home.propTypes = {
  goods: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      image: PropTypes.string,
      isFavourite: PropTypes.bool,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    })
  ),
  addToCart: PropTypes.func.isRequired,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,
  modal: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

};
Home.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};

export default Home;
