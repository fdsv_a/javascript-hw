import React from "react";
import PropTypes from "prop-types";
import Navigation from "../Navigation/Navigation";

const Header = (props) => {
  const { cartCounter, favoriteCounter } = props;
  return (
    <Navigation cartCounter={cartCounter} favoriteCounter={favoriteCounter} />
  );
};
Header.propTypes = {
  cartCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  favoriteCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
Header.defaultProps = {
  cartCounter: localStorage.getItem("cartCounter"),

  favoriteCounter: localStorage.getItem("favoriteCounter"),
};

export default Header;

