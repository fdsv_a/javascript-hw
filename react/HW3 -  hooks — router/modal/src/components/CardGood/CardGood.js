import React from "react";
import styles from "./CardGood.module.scss";
import PropTypes from "prop-types";
import { ReactComponent as Favourite } from "./images/star.svg";
import { useState } from "react";

const CardGood = (props) => {
  const [favorite, setFavorite] = useState(false);

  const {
    key,
    title,
    price,
    image,
    isFavorite,
    addToFavorite,
    deleteFavorite,
    id,
    showModal,
    deleteCart,
    addCart,
  } = props;

  return (
    <div className={styles.card}>
      <span>
        <img src={image} alt="" />

        {deleteCart && (
          <div
            onClick={()=>{showModal(id)}}
          >
            <span>&#10006;</span>
          </div>
        )}
      </span>
      <p className={styles.title}>{title}</p>
      <div className={styles.btnContainer}>
        <Favourite
          className={styles.favorite}
          onClick={() => {
            if (localStorage.getItem(id)&&localStorage.getItem(id).includes("favorite")) {
              setFavorite(false);
              deleteFavorite(id);
            } else {
              setFavorite(true);
              addToFavorite(id);
            }
          }}
          fill={
            (localStorage.getItem(id)&&localStorage.getItem(id).includes("favorite"))|| favorite === true
              ? "yellow"
              : "none"
          }
        />
        <span>Add to favorites</span>
      </div>
      <div className={styles.priceContainer}>
        <p className={styles.price}>
          <nobr>{price} &#8372;</nobr>
        </p>
        {addCart&&<button
          type="button"
          onClick={() => {
            console.log(id);
            showModal(id);
          }}
        >
          Add to cart
        </button>}
      </div>
    </div>
  );
};

CardGood.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  image: PropTypes.string,
  isFavorite: PropTypes.bool,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  showModal: PropTypes.func.isRequired,
  addCart: PropTypes.bool.isRequired,
  deleteCart: PropTypes.bool.isRequired,

};
CardGood.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};

export default CardGood;

