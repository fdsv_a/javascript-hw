import React from "react";
import CardGood from "../CardGood/CardGood";
import styles from "./ListGoods.module.scss";
import PropTypes from "prop-types";

const ListGoods = (props) => {
  const {
    goods,
    deleteCart,
    addCart,
    addToFavorite,
    deleteFavorite,
    showModal,
  } = props;
  return (
    <>
      <ul className={styles.cardList}>
        {goods.map(({ title, price, image, isFavorite, article, id }) => (
          <li key={id}>
            <CardGood
              id={id}
              article={article}
              title={title}
              price={price}
              image={image}
              isFavorite={isFavorite}
              addToFavorite={addToFavorite}
              deleteFavorite={deleteFavorite}
              deleteCart={deleteCart}
              addCart={addCart}
              showModal={showModal}
            />
          </li>
        ))}
      </ul>
    </>
  );
};

ListGoods.propTypes = {
  goods: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      image: PropTypes.string,
      isFavourite: PropTypes.bool,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    })
  ),
  addToCart: PropTypes.func.isRequired,
  addCart: PropTypes.bool.isRequired,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,
};
ListGoods.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,

};

export default ListGoods;
