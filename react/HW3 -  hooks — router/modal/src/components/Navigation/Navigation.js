import { NavLink } from "react-router-dom";
import styles from "./Navigation.module.scss";
import PropTypes from "prop-types";

const Navigation = (props) => {
  const { cartCounter, favoriteCounter } = props;

  return (
    <div className={styles.header}>
      <NavLink to="/">
        <img src="./images/logo.png" alt="" />
      </NavLink>
      <div className={styles.container}>
        <NavLink to="/cart">
          <img src="./images/cart.svg" alt="" />
        </NavLink>
        <div>
          {localStorage.getItem("cartCounter")
            ? localStorage.getItem("cartCounter")
            : cartCounter}
        </div>
        <NavLink to="/favorites">
          <img src="./images/star.svg" alt="" />
        </NavLink>
        <div>
          {localStorage.getItem("favoriteCounter")
            ? localStorage.getItem("favoriteCounter")
            : favoriteCounter}
        </div>
      </div>
    </div>
  );
};

Navigation.propTypes = {
  cartCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  favoriteCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
Navigation.defaultProps = {
  cartCounter: localStorage.getItem("cartCounter"),

  favoriteCounter: localStorage.getItem("favoriteCounter"),
};

export default Navigation;
