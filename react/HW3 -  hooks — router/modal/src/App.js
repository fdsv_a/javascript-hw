import React, { useEffect, useState } from "react";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import AppRoutes from "./AppRoutes";
import Preloader from "./components/Preloader/Preloader";

const App = () => {
  const [goods, setGoods] = useState([]);
  const [cartCounter, setCartCounter] = useState(0);
  const [favoriteCounter, setFavoriteCounter] = useState(0);
  const [loading, setLoading] = useState(true);
  const [modal, setModal] = useState();

  const addToCart = (id) => {
    let counter;
    localStorage.getItem("cartCounter")
      ? (counter = parseInt(localStorage.getItem("cartCounter")) + 1)
      : (counter = cartCounter + 1);
    setCartCounter(counter);
    localStorage.setItem("cartCounter", JSON.stringify(counter));
    localStorage.getItem(id) === "favorite"
      ? (localStorage[id] = ["cart", "favorite"])
      : (localStorage[id] = "cart");
  };
  const deleteFromCart = (id) => {
    let counter;
    localStorage.getItem("cartCounter")
      ? (counter = parseInt(localStorage.getItem("cartCounter")) - 1)
      : (counter = cartCounter - 1);
    setCartCounter(counter);
    localStorage.setItem("cartCounter", JSON.stringify(counter));
    localStorage.getItem(id).includes("cart") &&
    localStorage.getItem(id).includes("favorite")
      ? (localStorage[id] = "favorite")
      : localStorage.removeItem(id);
  };

  const closeModal = () => {
    setModal();
  };

  const showModal = (id) => {
    setModal(id);
  };
  const addToFavorite = (id) => {
    let counter;
    localStorage.getItem("favoriteCounter")
      ? (counter = parseInt(localStorage.getItem("favoriteCounter")) + 1)
      : (counter = favoriteCounter + 1);
    setFavoriteCounter(counter);
    localStorage.setItem("favoriteCounter", JSON.stringify(counter));
    localStorage.getItem(id) === "cart"
      ? (localStorage[id] = ["cart", "favorite"])
      : (localStorage[id] = "favorite");
  };

  const deleteFavorite = (id) => {
    let counter;
    localStorage.getItem("favoriteCounter")
      ? (counter = parseInt(localStorage.getItem("favoriteCounter")) - 1)
      : (counter = favoriteCounter - 1);
    setFavoriteCounter(counter);
    localStorage.setItem("favoriteCounter", JSON.stringify(counter));
    localStorage.getItem(id).includes(["cart", "favorite"])
      ? (localStorage[id] = "cart")
      : localStorage.removeItem(id);
  };

  useEffect(() => {
    const getGoods = async () => {
      const data = await fetch("./data.json").then((res) => res.json());
      setGoods(data);
      setLoading(false);
    };
    getGoods();
  }, []);
  return (
    <div className={styles.App}>
      <Header
        cartCounter={cartCounter && localStorage.getItem("cartCounter")}
        favoriteCounter={
          favoriteCounter && localStorage.getItem("favoriteCounter")
        }
      />
      {loading && <Preloader />}
      <div className={styles.container}>
        <AppRoutes
          goods={goods}
          addToFavorite={addToFavorite}
          deleteFavorite={deleteFavorite}
          addToCart={addToCart}
          deleteFromCart={deleteFromCart}
          showModal={showModal}
          closeModal={closeModal}
          modal={modal}
        />
      </div>
    </div>
  );
};

export default App;
