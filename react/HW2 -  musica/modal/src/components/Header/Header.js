import React from "react";
import styles from "./Header.module.scss";
import PropTypes from "prop-types";

class Header extends React.PureComponent {
  render() {
    const { cartCounter, favoriteCounter } = this.props;
    return (
      <div className={styles.header}>
        <img src="./images/logo.png" alt="" />
        <div className={styles.container}>
          <img src="./images/cart.svg" alt="" />
          <div>
            {localStorage.getItem("cartCounter")
              ? localStorage.getItem("cartCounter")
              : cartCounter}
          </div>
          <img src="./images/star.svg" alt="" />
          <div>
            {localStorage.getItem("favoriteCounter")
              ? localStorage.getItem("favoriteCounter")
              : favoriteCounter}
          </div>
        </div>
      </div>
    );
  }
}
Header.propTypes = {
  cartCounter: PropTypes.number,
  favoriteCounter: PropTypes.number,
};
Header.defaultProps = {
  cartCounter: localStorage.getItem("cartCounter"),

  favoriteCounter: localStorage.getItem("favoriteCounter"),
};

export default Header;
