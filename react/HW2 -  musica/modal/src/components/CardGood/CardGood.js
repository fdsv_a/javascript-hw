import React from "react";
import styles from "./CardGood.module.scss";
import PropTypes from "prop-types";
import { ReactComponent as Favourite } from "./images/star.svg";

class CardGood extends React.PureComponent {
  state = {
    isFavorite: null,
  };

  render() {
    const {
      key,
      title,
      price,
      image,
      isFavorite,
      addToFavorite,
      deleteFavorite,
      id,
      showModal,
    } = this.props;

    return (
      <div className={styles.card}>
        <img src={image} alt="" />
        <p className={styles.title}>{title}</p>
        <div className={styles.btnContainer}>
          <Favourite
            className={styles.favorite}
            onClick={() => {
              if (localStorage.getItem(id) === "true") {
                this.setState({ isFavorite: false });
                deleteFavorite(id);
              } else {
                this.setState({ isFavorite: true });

                addToFavorite(id);
              }
            }}
            fill={
              localStorage.getItem(id) === "true" ||
              this.state.isFavorite === true
                ? "yellow"
                : "none"
            }
          />
          <span>Add to favorites</span>
        </div>
        <div className={styles.priceContainer}>
          <p className={styles.price}>
            <nobr>{price} &#8372;</nobr>
          </p>
          <button
            type="button"
            onClick={() => {
              showModal();
            }}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
export default CardGood;

CardGood.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  image: PropTypes.string,
  isFavorite: PropTypes.bool,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  showModal: PropTypes.func.isRequired,
};
CardGood.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};
