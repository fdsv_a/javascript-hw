import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

class Modal extends React.PureComponent {
  render() {
    const { header, closeButton, text, actions, closeModal } = this.props;
    return (
      <div
        className={styles.container}
        onClick={(e) => {
          e.target === e.currentTarget && closeModal();
        }}
      >
        <div className={styles.modal}>
          <div className={styles.header}>
            <div>{header}</div>
            {closeButton && (
              <div onClick={closeModal}>
                <span>&#10006;</span>
              </div>
            )}
          </div>
          <div className={styles.text}>{text}</div>
          <div className={styles.button}>{actions}</div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.object.isRequired,
  closeModal: PropTypes.func.isRequired,
};
Modal.defaultProps = {
  header: "",
  closeButton: true,
  text: "",
};

export default Modal;
