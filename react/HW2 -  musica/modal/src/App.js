import React from "react";
import styles from "./App.module.scss";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import ListGoods from "./components/ListGoods/ListGoods";

class App extends React.PureComponent {
  state = {
    goods: [],
    showModal: false,
    cartCounter: 0,
    favoriteCounter: 0,
  };
  addToCart = () => {
    let cartCounter;
    localStorage.getItem("cartCounter")
      ? (cartCounter = parseInt(localStorage.getItem("cartCounter")) + 1)
      : (cartCounter = this.state.cartCounter + 1);
    this.setState(() => ({
      cartCounter: cartCounter,
    }));
    this.closeModal();
    localStorage.setItem("cartCounter", JSON.stringify(cartCounter));
  };
  deleteFavorite = (id) => {
    let favoriteCounter;
    localStorage.getItem("favoriteCounter")
      ? (favoriteCounter =
          parseInt(localStorage.getItem("favoriteCounter")) - 1)
      : (favoriteCounter = this.state.favoriteCounter - 1);
    this.setState(() => ({
      favoriteCounter: favoriteCounter,
    }));
    localStorage.setItem("favoriteCounter", JSON.stringify(favoriteCounter));
    localStorage.removeItem(id);
  };
  addToFavorite = (id) => {
    let favoriteCounter;
    localStorage.getItem("favoriteCounter")
      ? (favoriteCounter =
          parseInt(localStorage.getItem("favoriteCounter")) + 1)
      : (favoriteCounter = this.state.favoriteCounter + 1);
    this.setState(() => ({
      favoriteCounter: favoriteCounter,
    }));
    localStorage.setItem("favoriteCounter", JSON.stringify(favoriteCounter));
    localStorage.setItem(id, true);
  };
  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };
  showModal = () => {
    this.setState({
      showModal: true,
    });
  };

  async componentDidMount() {
    const data = await fetch("./data.json").then((res) => res.json());
    this.setState({ goods: data });
  }

  render() {
    return (
      <div className={styles.App}>
        <Header
          cartCounter={this.state.cartCounter}
          favoriteCounter={this.state.favoriteCounter}
        />
        <ListGoods
          goods={this.state.goods}
          addToFavorite={this.addToFavorite}
          deleteFavorite={this.deleteFavorite}
          addToCart={this.addToCart}
          showModal={this.showModal}
        />
        {this.state.showModal && (
          <Modal
            header="Do you want to add this item to your shopping cart?"
            closeButton={true}
            text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, animi nisi dolorum illum culpa quae placeat rem numquam a consequatur reprehenderit facere voluptas nostrum corporis aut eaque dolor sunt aperiam?"
            actions={
              <>
                <button onClick={this.addToCart}>Ok</button>
                <button onClick={this.closeModal}>Cancel</button>
              </>
            }
            closeModal={this.closeModal}
          />
        )}
      </div>
    );
  }
}

export default App;
