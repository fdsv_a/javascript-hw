"use srict";

let array = ["hello", "world", 23, "23", null, {}];
let str = "object";

const filterBy = (arr, type) => {
  return arr.filter((value) => typeof value !== type);
};

console.log(filterBy(array, str));
