"use strict";
const btnCollection = Array.from(document.querySelectorAll(".btn"));

document.addEventListener("keyup", (e) => {
  btnCollection.forEach((elem) => {
    elem.style.backgroundColor = "#000";

    if (
      e.code === `${elem.textContent}` ||
      e.code === `Key${elem.textContent}` ||
      e.key === `${elem.textContent}`
    ) {
      elem.style.backgroundColor = "#3a17d4";
    }
  });
});
