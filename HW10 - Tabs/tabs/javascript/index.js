"use strict";

const tabs = Array.from(document.querySelectorAll(".tabs-title"));
const content = Array.from(document.querySelectorAll(".content-title"));

const changeContentOnclick = () => {
  content.forEach((elem) => {
    tabs.forEach((elem) => {
      elem.classList.remove("active");
    });
    elem.classList.add("active");
  });
};

tabs.forEach((elem, index) => {
  elem.addEventListener("click", (elem) => {
    tabs.forEach((elem) => {
      elem.classList.remove("active");
    });
    elem.target.classList.add("active");
    console.log(index);
    content.forEach((elem) => {
      elem.classList.remove("active");
    });
    content[index].classList.add("active");
  });
});
