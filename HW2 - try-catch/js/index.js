const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class Elem {
  constructor() {
    this.list = document.createElement("ul");
    this.root = document.querySelector("#root");
  }
  createElem() {
    console.log(this.root);
    this.root.append(this.list);
    this.list.className = "list";
    books.forEach(({ author: a, name: n, price: p }) => {
      try {
        if (!a || !n || !p) {
          throw new ReferenceError("Данные некорректны");
        } else {
          this.listItem = document.createElement("li");
          this.list.append(this.listItem);
          this.listItem.className = "list__item";
          this.listItem.innerText = `Author: ${a}, Name: ${n}, Price: ${p}`;
        }
      } catch (err) {
        console.log(err.message);
      }
    });
  }
}
const bookCard = new Elem();
bookCard.createElem();
