const btn = document.querySelector("button");
btn.addEventListener("click", showIP);

async function showIP() {
  const response = await fetch("https://api.ipify.org/?format=json");
  const ip = await response.json();
  const { ip: ipValue } = ip;
  const responseIP = await fetch(
    `http://ip-api.com/json/${ipValue}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query`
  );
  const ipApi = await responseIP.json();
  const {
    continent: continentVal,
    country: countryVal,
    regionName: regionVal,
    city: cityVal,
    district: districtVal,
  } = ipApi;

  let span = document.querySelector("span");
  span.innerHTML = `
<p>${continentVal}</p>
<p>${countryVal}</p>
<p>${regionVal}</p>
<p>${cityVal}</p>
<p>${districtVal}</p>
`;
}
