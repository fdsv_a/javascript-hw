let btnSwitch = document.querySelectorAll(".btn");
let elementForChange = [];
const link = document.querySelectorAll(".list-item-link")
Array.prototype.push.apply(elementForChange, link)
elementForChange.push(document.querySelector(".title"));
const text = document.querySelectorAll(".text")
Array.prototype.push.apply(elementForChange, text)
elementForChange.push(document.querySelector(".button-position"));
elementForChange.push(document.querySelector(".button"));
elementForChange.push(document.querySelector(".footer"));
console.log(elementForChange);
btnSwitch.forEach((e) => {
  e.addEventListener('click', (e) => {
    if (e.currentTarget.classList.contains ("night")) {
        elementForChange.forEach((elem)=>{
            elem.classList.add('night');
            localStorage.setItem('button', 'night')
        })
        document.body.classList.add('night');
        e.currentTarget.style.display="none";
        document.querySelector('.day').style.display="block";
            }
if (e.currentTarget.classList.contains ("day")) {
    elementForChange.forEach((elem)=>{
        elem.classList.remove('night');
    })
    document.body.classList.remove('night');
    e.currentTarget.style.display="none";
    document.querySelector('.night').style.display="block";
    localStorage.setItem('button', 'day')

        }

    });
});

window.onload = () => {
    if(localStorage.getItem('button') === 'night'){
        elementForChange.forEach((elem)=>{
            elem.classList.add('night');
            localStorage.setItem('button', 'night')
        })
        document.body.classList.add('night');
        document.querySelector('.btn.night').style.display="none";
        document.querySelector('.day').style.display="block";

    }
    if(localStorage.getItem('button') === 'day'){
        elementForChange.forEach((elem)=>{
            elem.classList.remove('night');
        })
        document.body.classList.remove('night');
        document.querySelector('.btn.day').style.display="none";
        document.querySelector('.night').style.display="block";
        localStorage.setItem('button', 'day')
         
    }
}

